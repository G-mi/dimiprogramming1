/*
 * @author Dimitrios Mavromatis dima1894
 */

import java.util.ArrayList;

public class AuctionManager {

    private static final int SERIAL_NUMBER_DEFAULT_VALUE = 1;

    private static int serialNumberCounter = SERIAL_NUMBER_DEFAULT_VALUE;

    private ArrayList<Auction> auctions;

    private  ArrayList<Dog> dogList;

    private User[] users;

    public AuctionManager(ArrayList<Auction> act, ArrayList<Dog> dgl, User[] usr) {
        auctions = act;
        dogList = dgl;
        users = usr;
    }

    public void requestAuction() {
        String dogName = readDogName();
        if (!dogExist(dogName)) {
            System.out.println("Error: no such dog registered");
        } else {
            DogManager dogManager = new DogManager(dogList, users, auctions);
            Dog dog = dogManager.getDog(dogName);
            if (dogHasOwner(dog)) {
                System.out.println("Error: this dog already has an owner");
            }
            else if (dogIsAuctioned(dog.getName())) {
                System.out.println("Error: this dog is already up for auction");
            } else makeAuction(dog);
        }
    }

    private Auction generateAuction(Dog dog) {
        return new Auction(generateSerialNumber(), dog);
    }

    private String readDogName() {
        DogManager dogManager = new DogManager(dogList, users, auctions);
        return dogManager.readNameWithoutLoop();
    }

    private boolean dogExist(String dogName) {
        DogManager dogManager = new DogManager(dogList, users, auctions);
        return dogManager.dogInRegistry(dogName);
    }

    private boolean dogHasOwner(Dog dog) {
        return dog.getOwner() != null;
    }

    public boolean dogIsAuctioned(String dogName) {
        for (Auction auction: auctions) {
            if (namesMatch(auction.getDog().getName(), dogName)) {
                return true;
            }
        } return false;
    }

    private boolean namesMatch(String nameFirst, String nameSecond) {
        return nameFirst.toLowerCase().trim().equals(nameSecond.trim().toLowerCase());
    }

    private void makeAuction(Dog dog) {
        Auction auction = generateAuction(dog);
        placeDogInAuctionsList(auction);
        System.out.println(String.format("%s has been put up for auction in auction #%d", auction.getDog().getName(), auction.getSerialNumber()));
    }

    private void placeDogInAuctionsList(Auction auction) {
        auctions.add(auction);
    }

    private int generateSerialNumber() {
        return serialNumberCounter ++;
    }

    public Auction getAuction(Dog dog) {
        Auction auction = null;
        for (Auction i: auctions) {
            if (i.getDog().equals(dog)) {
                auction = i;
            }
        }
        return auction;
    }

    public void requestAuctionListing() {
        if (!auctionsAreInProgress()) {
            System.out.println("Error: no auctions in progress");
        } else {
            listAuctions();
        }
    }

    private boolean auctionsAreInProgress() {
        return auctions.size() != 0;
    }

    private void listAuctions() {
        for (Auction auction: auctions) {
            System.out.println(auction);
        }
    }

    public void requestAuctionClosing() {
        IO io = new IO();
        String dogName = IO.readString(IO.REQUEST_DOG_NAME);
        if (!dogIsAuctioned(dogName)) {
            System.out.println("Error: this dog is not up for auction");
        } else {
            DogManager dogManager = new DogManager(dogList, users, auctions);
            Dog dog = dogManager.getDog(dogName);
            Auction auction = getAuction(dog);
            if (auction.bidListIsEmpty(auction)) {
                System.out.println(String.format("The auction is closed. No bids where made for %s", dog.getName()));
                deleteAuction(auction);
            } else {
                closeAuction(auction, dog);
            }
        }
    }

    public void deleteAuction(Auction auction) {
        auctions.remove(auction);
    }

    private void closeAuction(Auction auction, Dog dog) {
        DogManager dogManager = new DogManager(dogList, users, auctions);
        UserManager userManager = new UserManager(users, dogList, auctions);
        DogDistributor dogDistributor = new DogDistributor(dogManager, userManager, users, dogList, auctions);
        dogDistributor.handleNewAuctionedOwnership(dog, auction.getLastBidUser());
        auction.getLastBidUser();
        System.out.println(String.format("The auction is closed. The winning bid was %dkr and was made by %s",
                auction.getLastBidAmount(), auction.getLastBidUser().getName()));
        deleteAuction(auction);
    }

    public boolean userMadeBid(User user) {
        for (Auction auction: auctions) {
            if (auction.getUser(user) != null) {
                if (auction.getUser(user).equals(user)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void deleteBid(User user) {
        for (Auction auction: auctions) {
            if (auction.getUser(user) != null) {
                if (auction.getUser(user).equals(user)) {
                    auction.deleteBid(auction, user);
                }
            }
        }
    }
}