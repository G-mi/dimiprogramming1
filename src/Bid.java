/*
 * @author Dimitrios Mavromatis dima1894
 */

public class Bid {

    private Auction auction;

    private User user;

    private Dog dog;

    private int amount;

    public Bid(Auction auction, User user, Dog dog) {
        this.auction = auction;
        this.user = user;
        this.dog = dog;
        this.amount = 0;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public User getUser() {
        return user;
    }

    @Override
    public String toString() {
        return String.format("User %s %d", user.getName(), amount);
    }
}
