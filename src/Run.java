/*
 * @author Dimitrios Mavromatis dima1894
 */

import java.util.ArrayList;

public class Run {
    private static final int DEFAULT_USERS_SIZE = 20;

    // All The Cases turned to constants for readability and avoidance of magic numbers/strings
    private static final String REGISTER_NEW_DOG = "registernewdog";
    private static final String REMOVE_DOG = "removedog";
    private static final String INCREASE_AGE = "increaseage";
    private static final String LIST_DOGS = "listdogs";
    private static final String REGISTER_NEW_USER = "registernewuser";
    private static final String LIST_USERS = "listusers";
    private static final String REMOVE_USER = "removeuser";
    private static final String GIVE_DOG = "givedog";
    private static final String START_AUCTION = "startauction";
    private static final String CLOSE_AUCTION = "closeauction";
    private static final String LIST_AUCTIONS = "listauctions";
    private static final String MAKE_BID = "makebid";
    private static final String LIST_BIDS = "listbids";
    private static final String EXIT = "exit";

    private ArrayList<Auction> auctions = new ArrayList<Auction>();
    private ArrayList<Dog> dogList = new ArrayList<Dog>();
    private User[] users = new User[DEFAULT_USERS_SIZE];

    private AuctionManager auctionManager = new AuctionManager(auctions, dogList, users);
    private BidManager bidManager = new BidManager(auctionManager, users, dogList, auctions);
    private DogManager dogManager = new DogManager(dogList, users, auctions);
    private UserManager userManager = new UserManager(users, dogList, auctions);

    private DogDistributor dogDistributor = new DogDistributor(dogManager, userManager, users, dogList, auctions);

    public static void main(String[] arg) {
        Run program = new Run();
        program.run();
    }

    private void startUp() {
        System.out.println("Welcome to Dimi's Dog Register!");
    }

    private void commandLoop() {
        String command;
        do {
            command = readCommand();
            handleCommand(command);
        } while (!command.equals("exit"));
    }

    private String readCommand() {
        System.out.print("Command?> ");
        IO input = new IO();

        return input.readString().toLowerCase().replaceAll("\\s", "");
    }

    private void handleCommand(String command) {

        switch (command) {
            case REGISTER_NEW_DOG:
                dogManager.registerNewDog();
                break;
            case REMOVE_DOG:
                dogManager.removeDog();
                break;
            case INCREASE_AGE:
                dogManager.increaseDogsAge();
                break;
            case LIST_DOGS:
                dogManager.listDogs();
                break;
            case REGISTER_NEW_USER:
                userManager.registerNewUser();
                break;
            case LIST_USERS:
                userManager.listUsers();
                break;
            case REMOVE_USER:
                userManager.removeUser();
                break;
            case GIVE_DOG:
                dogDistributor.giveDog();
                break;
            case START_AUCTION:
                auctionManager.requestAuction();
                break;
            case CLOSE_AUCTION:
                auctionManager.requestAuctionClosing();
                break;
            case LIST_AUCTIONS:
                auctionManager.requestAuctionListing();
                break;
            case MAKE_BID:
                bidManager.requestBid();
                break;
            case LIST_BIDS:
                bidManager.requestBidListing();
                break;
            case EXIT:
                break;
            default:
                System.out.println("Error: no such command!");
        }
    }

    private void closeDown() {
        System.out.println("Thanks, bye!");
    }

    public void run() {
        startUp();
        commandLoop();
        closeDown();
    }
}