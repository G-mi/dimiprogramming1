/*
 * @author Dimitrios Mavromatis dima1894
 */

import java.util.Scanner;

public class IO {

    public static final String REQUEST_USERNAME = "Enter the name of the user?> ";

    public static final String REQUEST_DOG_NAME = "Enter the name of the dog?> ";

    private static final Scanner INPUT = new Scanner(System.in);

    public static String readString(String requestedInputMessage) {
        System.out.print(requestedInputMessage);
        return INPUT.nextLine();
    }

    public String readString() {
        return INPUT.nextLine();
    }
    
    public int readInt() {
        int userInt = INPUT.nextInt();
        INPUT.nextLine();
        return userInt;
    }

    public double readDouble() {
        double userDouble = INPUT.nextDouble();
        INPUT.nextLine();
        return userDouble;
    }
}
