/*
 * @author Dimitrios Mavromatis dima1894
 */

import java.util.ArrayList;

public class BidManager {

    private AuctionManager auctionManager;

    private User[] users;

    private  ArrayList<Dog> dogList;

    private ArrayList<Auction> auctions;

    public BidManager(AuctionManager am, User[] usr, ArrayList<Dog> dgl, ArrayList<Auction> act) {
        auctionManager = am;
        users = usr;
        dogList = dgl;
        auctions = act;
    }

    public void requestBid() {
        String userName = IO.readString(IO.REQUEST_USERNAME);
        UserManager userManager = new UserManager(users, dogList, auctions);
        if (!userManager.userInRegister(userName)) {
            System.out.println("Error: no such user");
        } else {
            User user = userManager.getUser(userName);
            String dogName = getDogName();
            if (dogIsNotAuctioned(dogName)) {
                System.out.println("Error: this dog is not up for auction");
            }
            else {
                Dog dog = getRequestedDog(dogName);
                Auction auction = auctionManager.getAuction(dog);
                if ((!auction.bidListIsEmpty(auction) && userPlacedLastBid(user, dog)) || userHasBidRecord(user, dog)) {
                    Bid bid = makeBid(auction, user, dog);
                    replaceBid(auction, bid);
                }
                else {
                    Bid bid = makeBid(auction, user, dog);
                    placeBid(auction, bid);
                }
            }
        }
    }

    private boolean userPlacedLastBid(User user, Dog dog) {
        Auction auction = auctionManager.getAuction(dog);
        if (auction.getLastBidUser() == null || auction.getLastBidUser().equals(user)) {
            return true;
        }
        return false;
    }

    private boolean userHasBidRecord(User user, Dog dog) {
        Auction auction = auctionManager.getAuction(dog);
        return auction.hasBidRecord(auction, user);
    }

    private boolean dogIsNotAuctioned(String dogName) {
        return !auctionManager.dogIsAuctioned(dogName);
    }

    private String getDogName() {
        DogManager dogManager = new DogManager(dogList, users, auctions);
        return dogManager.readNameWithoutLoop();
    }

    private Dog getRequestedDog(String dogName) {
        DogManager dogManager = new DogManager(dogList, users, auctions);
        return dogManager.getDog(dogName);
    }

    private Bid makeBid(Auction auction, User user, Dog dog) {
        int amount = 0;
        Bid bid = new Bid(auction, user, dog);

        System.out.print(String.format("Amount to bid (min %d)?> ", auction.getLastBidAmount()));
        amount = readAmount(amount);
        while (amount <= auction.getLastBidAmount()) {
            System.out.println("Error: bid too low");
            System.out.print(String.format("Amount to bid (min %d)?> ", auction.getLastBidAmount()));
            amount = readAmount(amount);
        }
        bid.setAmount(amount);
        return bid;
    }

    private int readAmount(int amountToBeat) {
        IO input = new IO();
        return input.readInt();
    }

    private void replaceBid(Auction auction, Bid bid) {
        auction.replaceUserBids(auction, bid);
        System.out.println("Bid made");
        System.out.println(bid);
    }

    private void placeBid(Auction auction, Bid bid) {
        auction.setBidToAuction(bid);
        System.out.println("Bid made");
        System.out.println(bid);
    }

    public void requestBidListing() {
        String dogName = IO.readString(IO.REQUEST_DOG_NAME);
        if (dogIsNotAuctioned(dogName)) {
            System.out.println("Error: this dog is not up for auction");
        } else {
            DogManager dogManager = new DogManager(dogList, users, auctions);
            Dog dog = dogManager.getDog(dogName);
            Auction auction = auctionManager.getAuction(dog);
            if (auction.bidListIsEmpty(auction)) {
                System.out.println("No bids registered yet for this auction");
            } else {
                listBids(auction);
            }
        }
    }

    private void listBids(Auction auction) {
        auction.listBids(auction);
    }
}