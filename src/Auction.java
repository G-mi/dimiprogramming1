/*
 * @author Dimitrios Mavromatis dima1894
 */

import java.util.ArrayList;

public class Auction {

    private ArrayList<Bid> bids;

    private int serialNumber;

    private Dog dog;

    public Auction(int serialNumber, Dog dog) {
        this.serialNumber = serialNumber;
        this.dog = dog;
        this.bids = new ArrayList<>();

    }

    public boolean bidListIsEmpty(Auction auction) {
        return bids.size() == 0;
    }

    public Dog getDog() {
        return dog;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public int getLastBidAmount() {
        if (bids.size() == 0) {
            return 0;
        }
        return bids.get(0).getAmount();
    }

    public User getLastBidUser() {
        return bids.get(0).getUser();
    }

    public Bid getLastBid() {
        return bids.get(0);
    }

    public User getUser(User user) {
        User foundUser = null;
        for (int i = 0; i < bids.size(); i++) {
            if (bids.get(i).getUser() != null) {
                if (bids.get(i).getUser().equals(user)) {
                    foundUser = bids.get(i).getUser();
                    break;
                }
            }
        }
        return foundUser;
    }

    public Bid getBid(User user) {
        Bid requestedBid = null;
        for (Bid bid : bids) {
            if (bid.getUser() == user) {
                requestedBid = bid;
            }
        }
        return requestedBid;
    }

    public void deleteBid(Auction auction, User user) {
        Bid bid = null;
        int initialLengthStop = bids.size();
        for (int i = 0; i < initialLengthStop; i++) {
            bids.remove(auction.getBid(user));
        }
    }

    public boolean userMadeBid(User user) {
        User foundUser = null;
        for (int i = 0; i < bids.size(); i++) {
            if (bids.get(i).getUser().equals(user)) {
                foundUser = bids.get(i).getUser();
            }
        }
        return true;
    }

    public boolean noBidsMade() {
        return bids.size() == 0;
    }

    public void setBidToAuction(Bid bid) {
        bids.add(0, bid);
    }

    public void replaceUserBids(Auction auction, Bid bid) {
        if (bids.size() == 0) {
            bids.add(bid);
        }
        else {
            bids.add(0, bid);
            for (int i = 1; i < bids.size(); i++) {
                if (bids.get(i).getUser().equals(bid.getUser())) {
                    bids.remove(bids.get(i));
                }
            }
        }
    }

    public boolean hasBidRecord(Auction auction, User user) {
        for (Bid bid: bids) {
            if (bid.getUser().equals(user)) {
                return true;
            }
        } return false;
    }

    public void listBids(Auction auction) {
        for (Bid bid: bids) {
            System.out.println(bid);
        }
    }

    private String topBidsToPrint() {
        String topBids = "";
        int bidCount = 0;
        for (int i = 0; i < bids.size(); i++) {
            if (bids.get(i).getAmount() != 0) {
                topBids = topBids.concat(bids.get(i).toString().concat(", "));
                bidCount ++;
            } if (bidCount > 2) {
                break;
            }
        } if (topBids.length() == 0) {
            return topBids;
        } else return topBids.substring(0, topBids.length()-2);
    }

    @Override
    public String toString() {
        return String.format("Auction #%d: %s. Top bids:[%s]", serialNumber, dog.getName(), topBidsToPrint());
    }
}