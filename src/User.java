/*
 * @author Dimitrios Mavromatis dima1894
 */

import java.util.ArrayList;

public class User {

    private static final int DEFAULT_DOG_OWNERSHIP_SIZE = 20;

    private String name;

    private Dog[] dogsOwned;

    private ArrayList<Dog> dogList;

    private User[] users;

    private ArrayList<Auction> auctions;

    public User(String name, ArrayList<Dog> dgl, User[] urs, ArrayList<Auction> act) {
        this.name = name;
        dogList = dgl;
        users = urs;
        auctions = act;
        this.dogsOwned = new Dog[DEFAULT_DOG_OWNERSHIP_SIZE];
    }

    public String getDogsOwnedString(Dog[] dogsOwned) {
        String dogsOwnedString = "";
        for (int i = 0; i < dogsOwned.length; i++) {
            if (dogsOwned[i] != null) {
                dogsOwnedString = dogsOwnedString.concat(dogsOwned[i].getName()).concat(", ");
            }
        }
        return dogsOwnedString.substring(0, dogsOwnedString.length() - 2);
    }

    private Dog[] getDogsOwned() {
        return dogsOwned;
    }

    public String getName() {
        return name;
    }

    public boolean userIsDogOwner(User user) {
        Dog[] dogsOwned = user.getDogsOwned();
        for (Dog dog: dogsOwned) {
            if (dog != null) {
                return true;
            }
        }
        return false;
    }

    public boolean emptyOwnershipRegister() {
        for (Dog dog: dogsOwned) {
            if (dog != null) {
                return false;
            }
        }
        return true;
    }

    public static void placeDogToOwner(Dog dog, User user) {
        for (int i = 0; i < user.getDogsOwned().length; i++) {
            if (user.getDogsOwned()[i] == null) {
                user.getDogsOwned()[i] = dog;
                System.out.println(String.format("%s now owns %s", user.getName(), dog.getName()));
                break;
            }
        }
    }

    public static void placeAuctionedDogToOwner(Dog dog, User user) {
        for (int i = 0; i < user.getDogsOwned().length; i++) {
            if (user.getDogsOwned()[i] == null) {
                user.getDogsOwned()[i] = dog;
                break;
            }
        }
    }

    public void removeDogOwnerShip(Dog dog, User user) {
        Dog[] dogsOwned = user.getDogsOwned();
        for (int i = 0; i < dogsOwned.length; i++) {
            if (dogsOwned[i] != null) {
                if (dogsOwned[i].equals(dog)) {
                    dogsOwned[i] = null;
                    break;
                }
            }
        }
    }

    public void removeUserDogs(User user) {
        for (Dog dog: user.getDogsOwned()) {
            if (dog != null) {
                DogManager dogManager = new DogManager(dogList, users, auctions);
                dogManager.removeDog(dog);
            }
        }
    }

    @Override
    public String toString() {
        if (emptyOwnershipRegister()) {
            return String.format("* %s", name);
        }
        String dogsOwnedString = getDogsOwnedString(dogsOwned);
        return String.format("* %s [%s]", name, dogsOwnedString);
    }
}