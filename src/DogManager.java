/*
 * @author Dimitrios Mavromatis dima1894
 */

import java.util.ArrayList;

public class DogManager {

    private  ArrayList<Dog> dogList;

    private User[] users;

    private ArrayList<Auction> auctions;

    public DogManager(ArrayList<Dog> dgl, User[] usr, ArrayList<Auction> act) {
        dogList = dgl;
        users = usr;
        auctions = act;
    }

    public void registerNewDog() {

        Dog dog = generateDog();
        dogList.add(dog);
        System.out.println(String.format("%s added to register", dog.getName()));

    }

    public void removeDog(Dog dog) {
        AuctionManager auctionManager = new AuctionManager(auctions, dogList, users);
        if (auctionManager.dogIsAuctioned(dog.getName())) {
            Auction auction = auctionManager.getAuction(dog);
            auctionManager.deleteAuction(auction);
        }
        dogList.remove(dog);
    }

    public void removeDog() {
        if (emptyRegister()) {
            System.out.println("Error: no dogs in register");
        }
        else {
            String dogName = enterDogName();
            if (dogInRegistry(dogName)) {
                Dog dog = getDog(dogName);
                String actualName = dog.getName();
                System.out.println(String.format("%s is removed from the register", actualName));
                if (dog.getOwner() != null) {
                    UserManager userManager = new UserManager(users, dogList, auctions);
                    User user = userManager.getUser(dog.getOwner());
                    user.removeDogOwnerShip(dog, user);
                } AuctionManager auctionManager = new AuctionManager(auctions, dogList, users);
                if (auctionManager.dogIsAuctioned(dog.getName())) {
                    deleteAuction(auctionManager, dog);
                } dogList.remove(dog);
            } else System.out.println("Error: no such dog");
        }
    }

   private void deleteAuction(AuctionManager auctionManager, Dog dog) {
        Auction auction = auctionManager.getAuction(dog);
        auctionManager.deleteAuction(auction);
    }

    private String enterDogName() {
        IO input = new IO();
        System.out.print("Enter the name of the dog?> ");
        return input.readString();
    }

    public void increaseDogsAge() {

        if (emptyRegister()) {
            System.out.println("Error: no dogs registered");
        }

        else {
            Dog dog = null;

            String dogName = enterDogName();

            if (dogInRegistry(dogName)) {
                dog = getDog(dogName);
                dog.increaseAge();
                System.out.println(String.format("%s is now one year older", dogName));

            } else System.out.println("Error: no such dog");
        }
    }

    public boolean emptyRegister() {
        return dogList.size() == 0;
    }

    public void listDogs() {
        if (emptyRegister()) {
            System.out.println("Error: no dogs in register");
        }
        else {
            ArrayList<Dog> dogsMatched = new ArrayList<Dog>();
            double shortestTail = readShortestTail();
            for (Dog dog: dogList) {
                if (dog.getTailLength() >= shortestTail) {
                    dogsMatched.add(dog);
                }
            }
            new DogSorter().sort(dogsMatched);
            for (Dog dog: dogsMatched) {
                System.out.println(dog);
            }
        }
    }

    private double readShortestTail() {
        IO input = new IO();
        System.out.print("Smallest tail length to display?> ");
        return input.readDouble();
    }

    private Dog generateDog() {

        Dog dog = new Dog(readName(), readBreed(), readAge(), readWeight());

        return dog;
    }

    public String readName() {
        String name = null;
        do {
            IO input = new IO();
            System.out.print("Name?> ");
            name = input.readString();
        } while (invalidName(name));
        return name;
    }

    public String readNameWithoutLoop() {
        String name = null;
        IO input = new IO();
        System.out.print("Enter the name of the dog?> " );
        name = input.readString();
        return name;
    }

    public boolean nameNotFound(String name, boolean distributorKey) {
        return !dogInRegistry(name);
    }

    private boolean invalidName(String name) {
        if (isAWhiteSpaceName(name)) {
            System.out.println("Error: no white spaces for name");
            return true;
        }
        else if (dogInRegistry(name)) {
            System.out.println(String.format("Error: the name %s already exists!", name));
            return true;
        }  else return false;
    }

    private boolean isAWhiteSpaceName(String name) {
        return name.toLowerCase().trim().isEmpty();
    }

    private boolean invalidBreed(String breed) {
        if (isAWhiteSpaceName(breed)) {
            System.out.println("Error: no white spaces or empty input");
            return true;
        }
        else return false;
    }

    private String readBreed() {
        String breed = null;
        do {
            IO input = new IO();
            System.out.print("Breed?> ");
            breed = input.readString();
        } while (invalidBreed(breed));
        return breed;
    }

    private int readAge() {
        IO input = new IO();
        System.out.print("Age?> ");
        return input.readInt();
    }

    private int readWeight() {
        IO input = new IO();
        System.out.print("Weight?> ");
        return input.readInt();
    }

    public Dog getDog(String name) {
        Dog dog = null;
        for (Dog i: dogList) {
            if (i.getName().toLowerCase().trim().equals(name.toLowerCase().trim())) {
                dog = i;
                break;
            }
        } return dog;
    }

    private boolean namesMatch(String nameFirst, String nameSecond) {
        return nameFirst.toLowerCase().trim().equals(nameSecond.trim().toLowerCase());
    }

    public boolean dogInRegistry(String nameInput) {
        for (Dog dog: dogList) {
            if (namesMatch(dog.getName(), nameInput)) {
                return true;
            }
        } return false;
    }

    public boolean dogAlreadyOwned(String nameInput, boolean distributorKey) {
        Dog dog = getDog(nameInput);
        User owner = dog.getOwner();
        return owner != null;
    }
}