/*
 * @author Dimitrios Mavromatis dima1894
 */

import java.util.ArrayList;
import java.util.Arrays;

public class UserManager {

    private static final int EXPANDING_USERS_SIZE = 20;

    private User[] users;

    private ArrayList<Dog> dogList;

    private ArrayList<Auction> auctions;

    public UserManager(User[] usr, ArrayList<Dog> dgl, ArrayList<Auction> act) {
        users = usr;
        dogList = dgl;
        auctions = act;
    }

    public void removeUser() {
        if (emptyRegister()) {
            System.out.println("Error: no users in register");
        }
        else {
            String userName = enterUserName();
            if (userInRegister(userName)) {
                User user = getUser(userName);
                String actualName = user.getName();
                for (int i = 0; i < users.length -1; i++) {
                    if (users[i] != null && users[i].equals(user)) {
                        if (users[i].userIsDogOwner(users[i])){
                            user.removeUserDogs(users[i]);
                        }
                        AuctionManager auctionManager  = new AuctionManager(auctions, dogList, users);
                        if (auctionManager.userMadeBid(users[i])) {
                            auctionManager.deleteBid(users[i]);
                        }
                        users[i] = null;
                        capacityCheck();
                        break;
                    }
                }
                System.out.println(String.format("%s is removed from the register", actualName));
            } else System.out.println("Error: no such user");
        }
    }

    private void capacityCheck() {
        if (registerArrayNotOptimized()) {
            shrinkRegister();
        }
    }

    private String enterUserName() {
        IO input = new IO();
        System.out.print("Enter the name of the user?> ");
        return input.readString();
    }

    public void registerNewUser() {

        User user = generateUser();

        if (fullRegister()) {
            expandRegister();
        }

        for (int i = 0; i < users.length; i++) {
            if (users[i] == null) {
                users[i] = user;
                System.out.println(String.format("%s added to register", user.getName()));
                break;
            }
        }
    }

    private User generateUser() {

        User user = new User(readName(), dogList, users, auctions);

        return user;
    }

    public String readName() {
        String name = null;
        do {
            IO input = new IO();
            System.out.print("Name?> ");
            name = input.readString();
        } while (invalidName(name));
        return name;
    }

    public String readNameWithoutLoop() {
        String name = null;
        IO input = new IO();
        System.out.print("Enter the name of the new owner?> ");
        name = input.readString();
        return name;
    }

    public boolean nameNotFound(String name) {
        return !userInRegister(name);
    }

    private boolean invalidName(String name) {
        if (isAWhiteSpaceName(name)) {
            System.out.println("Error: no white spaces for name");
            return true;
        }
        else if (userInRegister(name)) {
            System.out.println(String.format("Error: the name %s already exists!", name));
            return true;
        }  else return false;
    }

    private boolean isAWhiteSpaceName(String name) {
        name += name.toLowerCase().replaceAll("\\s", "");
        return name.length() == 0 || name.equals(" ") || name.equals("\t") || name.equals("\n") || name.equals("\r");
    }

    public boolean userInRegister(String nameInput) {
        if (!emptyRegister()) {
            for (int i = 0; i < users.length; i++) {
                if (users[i] != null) {
                    if (namesMatch(users[i].getName(), nameInput)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean namesMatch(String nameFirst, String nameSecond) {
        return nameFirst.toLowerCase().replaceAll("\\s", "").equals(nameSecond.replaceAll("\\s", "").toLowerCase());
    }

    public boolean emptyRegister() {
        for (User user: users) {
            if (user != null) {
                return false;
            }
        }
        return true;
    }

    private boolean fullRegister() {
        for (int i = 0; i < users.length; i++) {
            if (users[i] == null) {
                return false;
            }
        } return true;
    }

    public void expandRegister() {
        users = Arrays.copyOf(users, users.length + EXPANDING_USERS_SIZE);
        System.out.println(String.format("Register capacity expanded to %d", users.length));
    }

    public void shrinkRegister() {
        users = Arrays.copyOf(users, users.length - EXPANDING_USERS_SIZE);
        System.out.println(String.format("Register capacity shrunk to %d", users.length));
    }

    public boolean registerArrayNotOptimized() {
        int nullCounter = 0;
        for (int i = 0; i < users.length; i++) {
            if (users[i] == null) {
                nullCounter ++;
            }
        }
        return nullCounter > EXPANDING_USERS_SIZE;
    }
    
    public void listUsers() {
        if (emptyRegister()) {
            System.out.println("Error: no user in register");
        }
        else {
            for (User user: users) {
                if (user != null) {
                    System.out.println(user);
                }
            }
        }
    }

    public User getUser(String name) {
        User user = null;
        for (User i: users) {
            if (i != null) {
                if (i.getName().toLowerCase().trim().equals(name.toLowerCase().trim())) {
                    user = i;
                    break;
                }
            }
        } return user;
    }

    public User getUser(User user) {
        for (User i: users) {
            if (i != null) {
                if (i.equals(user)) {
                    user = i;
                    break;
                }
            }
        } return user;
    }
}
