/*
 * @author Dimitrios Mavromatis dima1894
 */

import java.util.ArrayList;

public class DogDistributor {

    private User[] users;

    private ArrayList<Dog> dogList;

    private ArrayList<Auction> auctions;

    private DogManager dogManager;

    private UserManager userManager;

    public DogDistributor(DogManager dmg, UserManager urm, User[] urs, ArrayList<Dog> dgl, ArrayList<Auction> act) {
        dogManager = dmg;
        userManager = urm;
        users = urs;
        dogList = dgl;
        auctions = act;
    }

    public void giveDog() {

        if (dogManager.emptyRegister()) {
            System.out.println("Error: no dogs registered yet");
        }
        else if (userManager.emptyRegister()) {
            System.out.println("Error: no users registered yet");
        }
        else {
            String dogName = dogManager.readNameWithoutLoop();
            if (dogManager.nameNotFound(dogName, true)) {
                System.out.println("Error: no such dog");
            }
            else if(dogManager.dogAlreadyOwned(dogName, true)) {
                System.out.println("Error: dog already owned");
            }

            else {
                String userName = userManager.readNameWithoutLoop();
                if (userManager.nameNotFound(userName)) {
                    System.out.println("Error: no such user");
                }
                else {
                    handleNewOwnership(dogName, userName);
                }
            }
        }
    }

    public void handleNewOwnership(String dogName, String userName) {
        Dog dog = dogManager.getDog(dogName);
        User user = userManager.getUser(userName);
        User.placeDogToOwner(dog, user);
        dog.setOwner(user);
    }

    public void handleNewAuctionedOwnership(Dog dog, User user) {
        User.placeAuctionedDogToOwner(dog, user);
        dog.setOwner(user);
    }
}